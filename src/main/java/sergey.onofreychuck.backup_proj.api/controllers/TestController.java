package sergey.onofreychuck.backup_proj.api.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sergey on 10/20/16.
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping(value = "/load", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity load() {

        return ResponseEntity.ok("OK");
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity test() {

        return ResponseEntity.ok("OK test returned");
    }
}

